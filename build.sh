# Code to build the docker image used for the gitlab runner to speed things up

# Login on gitlab
docker login registry.gitlab.com

# Build the docker image
docker build -t registry.gitlab.com/microbial/docs .

# Push the docker image
docker push registry.gitlab.com/microbial/docs