# Introduction

## What is this book about?

This book is about the Microbial project. It is a collection of documents that describe the project and its components. The book is written in Markdown and built using [Jupyter Book](https://jupyterbook.org/intro.html). The book is hosted on [Gitlab](https://gitlab.com/microbial/docs) and is built using [Gitlab CI](https://docs.gitlab.com/ee/ci/). The book is published on [Gitlab Pages](https://docs.gitlab.com/ee/user/project/pages/). The book is available at [https://microbial.gitlab.io/docs/](https://microbial.gitlab.io/docs/). 

... bla bla bla ...