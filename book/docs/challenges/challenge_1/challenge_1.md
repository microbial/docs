# First challenge

## Description
I provide You with 2 objects - matrix of distance between samples (beta_ASV_bray_new) and some metadata (metadata_challenge) regarding these samples.

Load Your data in R session by executing this command: `load("data_challenge.RData").`

This dataset consists of samples taken from infants in 4 different timepoints.

The aim of this challenge is to create a data frame that would show the distance between timepoint 1 and 2, 1 and 3, 1 and 4 for the same infant, so more or less something like this:
 
![images](./images/table.jpg)
*Figure 1: Concept structure of the outcome*

## Data set

The data can be obtained [here](./data_challenge.RData).

## Solution

A solution can be found [here](./solution.ipynb).